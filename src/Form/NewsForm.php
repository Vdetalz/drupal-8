<?php
namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class NewsForm
 *
 * @package Drupal\news\Form
 */
class NewsForm extends FormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'news_add_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param null $news_id
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $news_id = NULL) {

    if ($news_id != NULL) {
      $news = $this->getEditValue($news_id);
    }
    $form['title']     = array(
      '#type'          => 'textfield',
      '#description'   => $this->t('Insert News shortcut name'),
      '#title'         => $this->t('News name'),
      '#default_value' => $news ? $news['title'] : '',
      '#required'      => TRUE,
    );
    $form['status']    = array(
      '#title'         => $this->t('News status'),
      '#description'   => $this->t('Chose news status'),
      '#type'          => 'select',
      '#options'       => array(
        0 => $this->t('Not published'),
        1 => $this->t('Published'),
      ),
      '#default_value' => $news ? $news['status'] : '',
      '#required'      => TRUE,
    );
    $form['sub_title'] = array(
      '#title'         => $this->t('News sub title'),
      '#description'   => $this->t('Insert News sub title'),
      '#default_value' => $news ? $news['sub_title'] : '',
      '#type'          => 'textfield',
    );

    $form['description']       = array(
      '#title'         => $this->t('News description'),
      '#description'   => $this->t('Insert News description'),
      '#default_value' => $news ? $news['description'] : '',
      '#type'          => 'textarea',
    );
    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = array(
      '#type'        => 'submit',
      '#value'       => $news ? $this->t('Update News') : $this->t('Add News'),
      '#button_type' => 'primary',
    );
    if ($news) {
      $form['id'] = array(
        '#type'  => 'value',
        '#value' => $news['nid'],
      );
    }
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('description'), 'utf-8') > 200) {
      $form_state->setErrorByName('description', $this->t('Description number is too long.'));
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $title       = $form_state->getValue('title');
    $status      = $form_state->getValue('status');
    $sub_title   = $form_state->getValue('sub_title');
    $description = $form_state->getValue('description');
    $val         = $form_state->getValue('id');

    /*Edit News*/
    if (isset($val)) {
      $node = Node::load($val);
      $node->set('title', $title);
      $node->set('status', $status);
      $node->set('newspages_sub_title', $sub_title);
      $node->set('newspages_description', $description);
      $node->save();
      drupal_set_message(t('News saved!'));
    }
    /*Add new data.*/
    else {
      $node = Node::create([
        'type'                  => 'newspages',
        'title'                 => $title,
        'status'                => $status,
        'newspages_sub_title'   => $sub_title,
        'newspages_description' => $description,
      ]);
      $node->save();
      drupal_set_message(t('News added!'));
    }
    /*Redirect to News list page*/
    $form_state->setRedirect('news_admin_list');
    return;
  }


  /**
   * @param $id
   *
   * @return array
   */
  public function getEditValue($id) {

    $news = [];
    if (isset($id)) {
      $node                = Node::load($id);
      $news['nid']         = $id;
      $news['title']       = $node->get('title')->value;
      $news['status']      = $node->get('status')->value;
      $news['sub_title']   = $node->get('newspages_sub_title')->value;
      $news['description'] = $node->get('newspages_description')->value;

    }
    return $news;
  }

}