<?php
/**
 * @file
 * Contains \Drupal\news\Plugin\Block\LastNewsBlock.
 */

namespace Drupal\news\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\Core\Link;

use Drupal\news\Model\NewsModel;


/**
 * Provides a 'LastNews' Block.
 *
 * @Block(
 *   id = "last_news_block",
 *   admin_label = @Translation("Last News block"),
 * )
 */
class LastNewsBlock extends BlockBase implements BlockPluginInterface {
  /**
   * Drupal\news\Model\NewsModel definition.
   *
   * @var \Drupal\news\Model\NewsModel
   */
  protected $model;

  public function setModel() {
    $this->model = new NewsModel();
  }

  /**
   * @return array
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['count'])) {
      $count = $config['count'];
    }
    else {
      $count = 1;
    }

    $this->setModel();
    $result = $this->model->loadNews($count);
    $items  = array();

    foreach ($result as $row) {
      $node       = Node::load($row->nid);
      $path       = '/newspages/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $sub  = $node->get('newspages_sub_title')->value;
      $desc = $node->get('newspages_description')->value;

      $items[] = array(
        'title'       => $title_link,
        'sub_title'   => $sub,
        'description' => $desc,
      );
    }

    return array(
      '#theme' => 'last_news_block',
      '#items' => $items,
    );
  }

  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'administer blocks');
  }

  /**
   * @return array
   */
  public function defaultConfiguration() {
    return array(
      'count' => 1,
    );
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['count'] = array(
      '#type'          => 'number',
      '#min'           => 1,
      '#title'         => t('How many news display'),
      '#default_value' => $config['count'],
    );

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $count = $form_state->getValue('count');

    if (!is_numeric($count) || $count < 1) {
      $form_state->setErrorByName('count', t('Needs to be an interger and more or equal 1.'));
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['count'] = $form_state->getValue('count');
  }
}