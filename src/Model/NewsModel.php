<?php
/**
 *
 */

namespace Drupal\news\Model;

/**
 * Class NewsModel.
 *
 * @package Drupal\news\Model.
 */
class NewsModel {
  /**
   * @param array|NULL $header
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadHeader(array $header = NULL) {

    $db    = \Drupal::database();
    $query = $db->select('node_field_data', 'n');
    $query->condition('type', 'newspages');
    $query->fields('n', ['nid', 'title', 'created', 'status']);

    if ($header) {
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
      $pager      = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
      $result     = $pager->execute();
    }
    else {
      $result = $query->execute();
    }
    return $result;
  }

  /**
   * @param $news_id
   */
  public function delete($news_id) {
    $news_deleted = \Drupal::database()->delete('node_field_data')
                           ->condition('nid', $news_id)
                           ->execute();
  }

  /**
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadNews( $count = NULL ) {
    $db    = \Drupal::database();
    $query = $db->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'title']);
    $query->condition('type', 'newspages');
    $query->condition('status', 1);
    if($count) {
      $query->range(0, $count);
    }
    $query->orderBy('created', 'DESC');
    $result =  $query->execute();

    return $result;
  }
}