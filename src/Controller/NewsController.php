<?php
/**
 *
 */

namespace Drupal\news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;

use Drupal\news\Model\NewsModel;

/**
 * Class NewsController.
 *
 * @package Drupal\news\Controller.
 */
class NewsController extends ControllerBase {

  /**
   * Drupal\news\Model\NewsModel definition.
   *
   * @var \Drupal\news\Model\NewsModel
   */
  protected $model;

  public function __construct() {
    $this->model = new NewsModel();
  }

  /**
   * @return array
   */
  public function newsListBuilder() {

    $header = array(
      array('data' => $this->t('Title'), 'field' => 'title', 'sort' => 'asc'),
      array('data' => $this->t('Date'), 'field' => 'created'),
      array('data' => $this->t('Status')),
      array('data' => $this->t('Sub Title')),
      array('data' => $this->t('Description')),
      array('data' => $this->t('Action')),
    );

    $result = $this->model->loadHeader($header);

    // Populate the rows.
    $rows = array();
    foreach ($result as $row) {

      $node = Node::load($row->nid);
      $sub  = $node->get('newspages_sub_title')->value;
      $desc = $node->get('newspages_description')->value;

      $path      = '/admin/config/content/news/edit/' . $row->nid;
      $edit      = Url::fromUri('internal:' . $path);
      $edit_link = Link::fromTextAndUrl(t('Edit'), $edit)->toString();

      $path     = '/admin/config/content/news/delete/' . $row->nid;
      $del      = Url::fromUri('internal:' . $path);
      $del_link = Link::fromTextAndUrl(t('Delete'), $del)->toString();

      $path       = '/newspages/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $mainLink = $this->t('@edit | @delete', array(
        '@edit'   => $edit_link,
        '@delete' => $del_link,
      ));

      $row->status == '1' ? $status = 'Published' : $status = 'Not published';

      $rows[] = array(
        array('data' => $title_link),
        array('data' => date('Y-m-d', $row->created)),
        array('data' => $status),
        array('data' => $sub),
        array('data' => $desc),
        array('data' => $mainLink),
      );
    }

    // The table description.
    $build = array(
      '#markup' => $this->t('News List'),
    );

    // Generate the table.
    $build['config_table'] = array(
      '#theme'  => 'table',
      '#header' => $header,
      '#rows'   => $rows,
    );

    // Finally add the pager.
    $build['pager'] = array(
      '#type' => 'pager',
    );

    return $build;

  }

  /**
   * @param $news_id
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteNews($news_id) {
    $this->model->delete($news_id);

    drupal_set_message($this->t('News deleted!'));
    return new \Symfony\Component\HttpFoundation\RedirectResponse(\Drupal::url('news_admin_list'));
  }

  /**
   * @return array
   */
  public function archive() {
    $items  = array();
    $result = $this->model->loadNews();

    foreach ($result as $row) {
      $node       = Node::load($row->nid);
      $path       = '/newspages/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $sub  = $node->get('newspages_sub_title')->value;
      $desc = $node->get('newspages_description')->value;

      $items[] = array(
        'title'       => $title_link,
        'sub_title'   => $sub,
        'description' => $desc,
      );
    }

    return array(
      '#theme' => 'newsarchive',
      '#items' => $items,
    );
  }

  /**
   * @param $news_id
   *
   * @return array
   */
  public function page($news_id) {
    $items = array();
    $node  = Node::load($news_id);

    $items['item'] = array(
      'title'       => $node->getTitle(),
      'sub_title'   => $node->get('newspages_sub_title')->value,
      'description' => $node->get('newspages_description')->value,
    );

    return array(
      '#theme' => 'newspage',
      '#items' => $items,
    );
  }
}