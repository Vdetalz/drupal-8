<?php
/**
 *
 */

namespace Drupal\news\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the comment entity.
 *
 * @see \Drupal\comment\Entity\Comment.
 */
class NewsAccessControlHandler extends EntityAccessControlHandler {
  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit news');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete news');
    }
    return AccessResult::allowed();
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param array $context
   * @param null $entity_bundle
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add news');
  }
}